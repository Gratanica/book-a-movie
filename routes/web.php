<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CheckAvailabilityController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BookTicket;
use App\Http\Controllers\CancelBookingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ HomeController::class,'index']);

Auth::routes();

Route::get('/home', [ HomeController::class,'index']);

//show user dashboard, tickets booked
Route::get('/admin', [AdminController::class, 'dashboard']);
Route::get('/admin_ticket_cancellation', [AdminController::class, 'dashboardCancellation']);

//handler ajax request hits endpoint, to check availability
Route::post('/checkDateAvailability', [CheckAvailabilityController::class,'checkDateAvailability']);
Route::post('/checkTimeSlotAvailability', [CheckAvailabilityController::class,'checkTimeSlotAvailableSeats']);

//making a booking
Route::post('/make_booking', [BookTicket::class,'index']);
Route::post('/cancel_booking', [CancelBookingController::class,'index']);
