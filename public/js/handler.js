$(document).ready(function()
{

    $('.timeslot_time').prop('disabled', true);
    $('.bookTicket').prop('disabled', true);
    $('.tickets').prop('disabled', true);

    //incase you switch a movie at a later stage, then we need to reset all values below.
    $('input[name=movieSelection]').change(function()
    {
        $('.timeslot_time').prop('disabled', true);
        $('.bookTicket').prop('disabled', true);
        $('.tickets').prop('disabled', true);
    });

    // When Selecting a date, we need to check that the date is available.
    $('.timeslot_date').change(function()
    {
        var date = $(this).val();
        var movie_id = $('input[name=movieSelection]:checked').val();

        $.ajax({
            url: "/checkDateAvailability",
            dataType: "json", //must return jSON
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "Post",
            async: true,
            data: {
                'date' : date,
                'movie_id' : movie_id
            },
            success: function (data)
            {
                if(typeof data.time_slots !== "undefined" && data.is_available === true)
                {
                    //there is an available date
                    $.each(data.time_slots, function (key, value) {
                        //foreach loop
                        $('.timeslot_time').append($('<option>', {
                            value: value.times_slot_id,
                            text: value.slot
                        }));

                    });

                    $('.timeslot_time').prop('disabled', false);

                }else
                {
                    $('#fullyBooked').show();
                }
            },
            error: function (xhr, exception) {
                $('#fullyBooked').show().val('Failed to collect data.');
            }
        });
    });

    // When selecting a timeslot, we need to display how much seats are still available.
    $('.timeslot_time').change(function()
    {
        var time_slot_id = $(this).val();

        $.ajax({
            url: "/checkTimeSlotAvailability",
            dataType: "json", //must return jSON
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "Post",
            async: true,
            data: {
                'slot' : time_slot_id,
            },
            success: function (data)
            {
                if(typeof data.available_seats !== "undefined")
                {
                    $('.tickets').prop('disabled', false);
                    $('.tickets').prop('max', data.available_seats);
                    $('.bookTicket').prop('disabled', false);
                }else
                {
                    $('#fullyBooked').show();
                }
            },
            error: function (xhr, exception) {
                $('#fullyBooked').show().val('Failed to collect data.');
            }
        });
    });

    $('#jupiter_theater').change(function()
    {
        var theater = $(this).val();

        $('.collapseMovies').collapse('hide');
        $('#'+theater).collapse('show');
    });

    $('#mars_theater').change(function()
    {
        var theater = $(this).val();

        $('.collapseMovies').collapse('hide');
        $('#'+theater).collapse('show');
    });

    <!-- intilize datatable -->
    $('#datatable').DataTable();

});

