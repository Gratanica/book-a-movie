# Book a Ticket At My Cinema


### Requirements
```
docker
docker-compose
```

### Get the files

Clone this project using the following commands:

```
git clone https://Gratanica@bitbucket.org/Gratanica/book-a-movie.git cinema_app
cd cinema_app
```

Create and update Your .env file

```
Copy the .env.example file and create a .env file.
cp .env.example .env

Find the block that specifies DB_CONNECTION and update it to reflect the specifics of your setup. You will modify the following fields:

DB_HOST will be your db database container.
DB_DATABASE will be the laravel database.
DB_USERNAME will be the username you will use for your database. In this case, we will use laraveluser.
DB_PASSWORD will be the secure password you would like to use for this user account.

```

Spin up the containers 

```
docker-compose up -d --build
```


Install Node Package Manager

```
cd cinema_app
npm install
```

Generate a key 

```
docker-compose exec app php artisan key:generate
```
Clear Cache
```
docker-compose exec app php artisan config:cache
```

###Configure DB

```
docker-compose exec db bash
mysql -u root -p
GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'your_laravel_db_password';
FLUSH PRIVILEGES;
```

Setup your tables.

```
docker-compose exec app php artisan migrate
```
Seed relational data.

```
docker-compose exec app php artisan db:seed --class=ImportTheaterData
```


If you run into any issues:

```
You can update your host to db or localhost.:
#DB_HOST=db
DB_HOST=localhost

You can ensure cache and configs are clear by running the following
docker-compose exec app php artisan cache:clear
docker-compose exec app php artisan config:clear
```

All data should now be populated and ready to use...

You can now load the site using localhost
