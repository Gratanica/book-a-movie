<?php

namespace Database\Seeders;

use App\Models\Theater;
use App\Models\Location;
use App\Models\Movie;
use App\Models\Timeslot;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class ImportTheaterData extends Seeder
{

    protected $location_one_id;

    protected $location_two_id;

    protected $timeslot_one = '09:30:00';

    protected $timeslot_two = '12:15:00';

    protected $timeslot_three = '14:45:00';

    protected $timeslot_four = '17:05:00';

    protected $timeslot_five = '19:35:00';

    /**
     * Run the database seeds.
     * The purpose of this method is to ensure we populate relational data into the database tables.
     *
     * @return void
     */
    public function run()
    {
        //we add the locations
        $this->addLocationOne();
        $this->addLocationTwo();

        $theaters = $this->getTheaters();

        $movies = $this->getMovies();

        //we separate the movies into chunks of 4 arrays of two items each (2 movies per theatre)
        $theater_movies = array_chunk($movies, '2');

        //set chunk counter
        $chunk_index = 0;

        foreach($theaters as $theater)
        {
            //insert Theater
            $theater_insert = Theater::updateOrCreate($theater, $theater);

            //check if it worked
            if($theater_insert->recentlyCreated || $theater_insert->exists)
            {
                //we loop through chunk of two movies.
                foreach ($theater_movies[$chunk_index] as $theater_movie)
                {
                    //we set the foreign key for the associated theater_id
                    $movie_row = Arr::add($theater_movie, 'theater_id', $theater_insert->id);

                    //insert movie
                    $movie_insert = Movie::updateOrCreate($movie_row, $movie_row);

                    //check if movie was added
                    if($movie_insert->recentlyCreated || $movie_insert->exists)
                    {
                        //now we add timeslots for each movie.
                        //Movies only showing in October (should create enough slots)
                        $this->addMonthlyTimeslots($movie_insert->id);

                    }
                }

                //Move to next chunk
                $chunk_index++;
                continue;
            }

            echo 'Failed to import data';
            exit;
        }
    }

    private function getMovies()
    {
        return [
            [
                'title' => 'Venom: Let There Be Carnage (2021)',
                'description' => 'Tom Hardy returns to the big screen as the lethal protector Venom, one of Marvel\'s greatest and most complex characters.',
                'img_url' => 'img/venom_movie.jpg',
            ],
            [
                'title' => 'The Addams Family 2 (2021)',
                'description' => 'The Addams get tangled up in more wacky adventures and find themselves involved in hilarious run-ins with all sorts of unsuspecting characters. Sequel to the 2019 animated film, \'The Addams Family\'.',
                'img_url' => 'img/adams_family2_movie.jpg',
            ],
            [
                'title' => 'Mayday (2021)',
                'description' => 'Ana is transported to a dreamlike and dangerous land where she joins an army of girls engaged in a never-ending war. Though she finds strength in this exhilarating world, she realizes that she\'s not the killer they want her to be.',
                'img_url' => 'img/mayday_movie.jpg',
            ],
            [
                'title' => 'Old Henry (2021)',
                'description' => 'An action western about a farmer who takes in an injured man with a satchel of cash. When a posse comes for the money, he must decide who to trust. Defending a siege, he reveals a gunslinging talent calling his true identity into question.',
                'img_url' => 'img/old_henry_movie.jpg',
            ],
            [
                'title' => 'The Many Saints of Newark (2021)',
                'description' => 'A look at the formative years of New Jersey gangster Tony Soprano.c',
                'img_url' => 'img/the_many_saints_movie.jpg',
            ],
            [
                'title' => 'Titane (2021)',
                'description' => 'Following a series of unexplained crimes, a father is reunited with the son who has been missing for 10 years. Titane : A metal highly resistant to heat and corrosion, with high tensile strength alloys.',
                'img_url' => 'img/titane_movie.jpg',
            ],
            [
                'title' => 'Survive the Game (2021)',
                'description' => 'A Man\'s life on his farm is interrupted when a cop and a pair of dangerous criminals show up.',
                'img_url' => 'img/survive_the_game_movie.jpg',
            ],
            [
                'title' => 'Halloween Kills (2021)',
                'description' => 'The saga of Michael Myers and Laurie Strode continues in the next thrilling chapter of the Halloween series.',
                'img_url' => 'img/halloween_kills_movie.jpg',
            ]
        ];
    }

    private function addLocationOne()
    {
        $this->location_one_id = Location::updateOrCreate(['name' => 'Mars'],[ 'name' => 'Mars'])->id;
    }

    private function addLocationTwo()
    {
        $this->location_two_id = Location::updateOrCreate(['name' => 'Jupiter'],[ 'name' => 'Jupiter'])->id;
    }

    private function getTheaters()
    {
        return [
            [
                'location_id' => $this->location_one_id,
                'alias' => 'TBT',
                'name' => 'The Biggest Theatre (TBT)',
            ],
            [
                'location_id' => $this->location_one_id,
                'alias' => 'TBBT',
                'name' => 'The Biggest Big Theatre (TBBT)',
            ],
            [
                'location_id' => $this->location_two_id,
                'alias' => 'FT',
                'name' => 'Flaming Theatre (FT)',
            ],
            [
                'location_id' => $this->location_two_id,
                'alias' => 'AT',
                'name' => 'Alien Theatre (AT)',
            ]
        ];
    }

    private function addMonthlyTimeslots($movie_id)
    {
        //we initialize array with default vars.
        $timeslot_array = [
            'movie_id' => $movie_id,
            'seat_limit' => 30,
            'seats_taken' => 0,
        ];

        //we get teh amount of days left for October
        $end_of_the_month = Carbon::now()->endOfMonth();
        $count_days_of_month_left = Carbon::now()->diffInDays($end_of_the_month, false);

        //we capture todays date
        $date = Carbon::now();

        //we add timeslots for the whole of October for each movie.
        for($i = $date->day; $i <= $count_days_of_month_left+$date->day; $i++)
        {

            //we add 5 timeslots per day
            for($slot = 0; $slot < 5; $slot++)
            {
                $this->addTimeSlot($date->format('Y-m-d'),$timeslot_array, $this->timeslot_one);
                $this->addTimeSlot($date->format('Y-m-d'),$timeslot_array, $this->timeslot_two);
                $this->addTimeSlot($date->format('Y-m-d'),$timeslot_array, $this->timeslot_three);
                $this->addTimeSlot($date->format('Y-m-d'),$timeslot_array, $this->timeslot_four);
                $this->addTimeSlot($date->format('Y-m-d'),$timeslot_array, $this->timeslot_five);
            }

            $date = $date->addDay();
        }

    }

    private function addTimeSlot($day, $timeslot_array, $time)
    {
        $timestamp = $this->generateDateTimeStamp($day, $time);
        $movie_timeslot = Arr::add($timeslot_array, 'time_start', $timestamp);
        Timeslot::updateOrCreate($movie_timeslot, $movie_timeslot);
    }

    private function generateDateTimeStamp($date, $timeslot)
    {
        //return Carbon::createFromFormat('Y-m-d H:i:s', '2016-01-23 11:53:20');
        return Carbon::createFromFormat('Y-m-d H:i:s', "{$date} {$timeslot}");
    }

}
