@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <h2 class="alert alert-success">Successfully booked your movie.</h2>
                <div class="card p-3 text-center">
                    <div class="card-header">
                        {{ $booking_result['movie_info']['title'] }}
                    </div>
                    <div class="card-body">
                        <img src="{{ asset($booking_result['movie_info']['img_url']) }}" class="card-img-top" alt="..." />
                        <div class="alert alert-info text-center">
                            Your booking reference: {{ $booking_result['booking_reference'] }}
                        </div>
                    </div>
                    <div class="card-footer">
                        <p class="card-text">
                            {{ $booking_result['movie_info']['description'] }}
                        </p>
                        <a href="/admin"> >>> Click to go to your dashboard! <<< </a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
