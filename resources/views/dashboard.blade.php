@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            {{--here we will display the users tickets booked--}}
            <section class="col-sm-6 col-md-12">
                <br />
                <h3 class="text-center mb-4">Tickets booked</h3>
                <hr class="my-4 shadow-lg" />
                <br />
                <div id="content_padded">
                    <div id="content">
                        <div class="table-responsive">
                            <table id="datatable" class="display cell-border compact dt-responsive">
                                <thead>
                                    <tr>
                                        <th>Movie title</th>
                                        <th>Tickets booked</th>
                                        <th>Movie time start</th>
                                        <th>Location</th>
                                        <th>Theater</th>
                                        <th>Date booked</th>
                                        <th>Status</th>
                                        <th>Reference</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($bookings as $booking)
                                    <tr>
                                        <td>{{ $booking['title'] }}</td>
                                        <td>{{ $booking['tickets'] }}</td>
                                        <td>{{ $booking['start_time'] }}</td>
                                        <td>{{ $booking['location'] }}</td>
                                        <td>{{ $booking['theater'] }}</td>
                                        <td>{{ $booking['date'] }}</td>
                                        <td>{{ $booking['status'] }}</td>
                                        <td>{{ $booking['reference'] }}</td>
                                    </tr>
                                @endforeach
                                <tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
