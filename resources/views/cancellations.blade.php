@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row justify-content-center">
                {{--here we will display the users tickets booked--}}
                <section class="col-sm-6 col-md-12">
                    <br />
                    <h3 class="text-center mb-4">Ticket Cancellations</h3>
                    <hr class="my-4 shadow-lg" />
                    <br />
                    <!-- Start accordion -->
                    <div id="accordion" class="">
                        @if($message['message_type'] == 'no data')
                            <div class="alert alert-warning">
                                {{ $message['message'] }}
                            </div>
                        @elseif($message['message_type'] == 'success')
                            <div class="alert alert-success">
                                {{ $message['message'] }}
                            </div>
                        @elseif($message['message_type'] == 'error')
                            <div class="alert alert-danger">
                                {{ $message['message'] }}
                            </div>
                        @endif
                        <div class="p-3 text-center">
                            <div class="blockquote">
                                <button data-toggle="collapse" data-target="#cancel_booking" class="btn btn-lg btn-primary">Cancel A booking</button>
                            </div>
                        </div>
                        <div class="collapse" id="cancel_booking" data-parent="#cancel_booking">
                            <div class="well">
                                <form action="cancel_booking" method="post" >
                                    @csrf
                                    <div class="form-group">
                                        <label for="cancel_id">Available Bookings to Cancel</label>
                                        <select name="cancel_id" class="form-control" required>
                                            <option selected="selected"></option>
                                            @foreach($tickets as $ticket)
                                                <option value="{{ $ticket['booking_id'] }}">{{ $ticket['movie_name'] }} - {{ $ticket['ticket_count'] }} tickets @ {{ $ticket['time_start'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <input class="btn-primary" type="submit" value="Proceed">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
