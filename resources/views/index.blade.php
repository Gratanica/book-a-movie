@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="mt-4 p-5 rounded text-center">
            <h1>Welcome to my view theater</h1>
        </div>


        <div id="myGroup" class="jumbotron text-center">
            @if($booking_result['message_type'] == 'no data')
                <div class="alert alert-warning">
                    {{ $booking_result['message'] }}
                </div>
            @elseif($booking_result['message_type'] == 'success')
                <div class="alert alert-success">
                    {{ $booking_result['message'] }}
                </div>
            @elseif($booking_result['message_type'] == 'error')
                <div class="alert alert-danger">
                    {{ $booking_result['message'] }}
                </div>
            @endif
            <div class="card-header">
                <h3>Select your location</h3>
            </div>

            <div class="row w-100">
                <div class="col-md-6">
                    <div class="p-3 text-center">
                        <div class="blockquote">
                            <button id="btn_jupiter_collapse" data-toggle="collapse" data-target="#jupiter" class="btn btn-primary">Jupiter</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-3 text-center">
                        <div class="blockquote">
                            <button id="btn_mars_collapse" data-toggle="collapse" data-target="#mars" class="btn btn-primary">Mars</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="accordion-group">

                <div class="collapse" id="jupiter" data-parent="#myGroup">
                    <div class="well">
                        <form action="make_booking" method="post" >
                            @csrf
                            <div class="form-group">
                                <input type="text" class="destination" style="display: none" value="jupiter">
                                <label for="theater">Select Your Theater</label>
                                <select name="theater" class="form-control" id="jupiter_theater" required>
                                    <option selected="selected"></option>
                                    @foreach($data['jupiter_theaters'] as $theater)
                                        <option value="{{ $theater['alias'] }}">{{ $theater['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="collapse collapseMovies" id="FT">
                                    <label for="jupiter_movie">Select Your Movie</label>
                                    <label for="jupiter_movie">Flaming Theatre</label>
                                    <div class="row radio text-center">
                                        @foreach($data['jupiter_movies'] as $movie)
                                            @if($movie['alias'] == 'FT')
                                                <div class="card col-md-6 p-3 text-center h-100">
                                                    <div class="card-header">
                                                        {{ $movie['title'] }}
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="jupiter_{{ $movie['id'] }}" style="cursor: pointer">
                                                            <img src="{{ asset($movie['img_url']) }}" class="card-img-top" alt="..." />
                                                        </label>
                                                        <input type="radio" name="movieSelection" id="jupiter_{{ $movie['id'] }}" value="{{ $movie['id'] }}">
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="card-text">
                                                            {{ $movie['description'] }}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                <div class="collapse collapseMovies" id="AT">
                                    <label for="jupiter_movie">Select Your Movie</label>
                                    <label for="jupiter_movie">Alien Theatre</label>
                                    <div class="row radio text-center">
                                        @foreach($data['jupiter_movies'] as $movie)
                                            @if($movie['alias'] == 'AT')
                                                <div class="card col-md-6 p-3 text-center h-100">
                                                    <div class="card-header">
                                                        {{ $movie['title'] }}
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="jupiter_{{ $movie['id'] }}" style="cursor: pointer">
                                                            <img src="{{ asset($movie['img_url']) }}" class="card-img-top" alt="..." />
                                                        </label>
                                                        <input type="radio" name="movieSelection" id="jupiter_{{ $movie['id'] }}" value="{{ $movie['id'] }}">
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="card-text">
                                                            {{ $movie['description'] }}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="jupiter_timeslot">Select Your Time Slot</label>
                                <input name="date_booked" type="date" min="{{ date("Y-m-d") }}" max="2021-10-31" class="form-control timeslot_date" required>
                                <select name="time_slot_id" class="form-control timeslot_time" required>
                                </select>
                                <div class="alert alert-danger" style="display: none" id="fullyBooked">
                                    <p>Sorry, date fully booked</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jupiter_tickets">Select Number Of Tickets</label>
                                <input name="ticket_count" type="range" min="1" max="30" value="1" class="form-control-range tickets" onInput="$('#jupiter_rangeval').html($(this).val())">
                            </div>
                            <div class="form-group text-center">
                                <span id="jupiter_rangeval">1</span>
                            </div>
                            <div>
                                <input class="bookTicket" type="submit" value="Book Now">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="collapse" id="mars" data-parent="#myGroup">
                    <div class="well">
                        <form action="make_booking" method="post" >
                            @csrf
                            <div class="form-group">
                                <input type="text" class="destination" style="display: none" value="mars">
                                <label for="theater">Select Your Theater</label>
                                <select name="theater" class="form-control" id="mars_theater" required>
                                    <option selected="selected"></option>
                                    @foreach($data['mars_theaters'] as $theater)
                                        <option value="{{ $theater['alias'] }}">{{ $theater['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="collapse collapseMovies" id="TBBT">
                                    <label for="mars_movie">Select Your Movie</label>
                                    <label for="mars_movie">The Biggest Big Theatre</label>
                                    <div class="row radio text-center">
                                        @foreach($data['mars_movies'] as $movie)
                                            @if($movie['alias'] == 'TBBT')
                                                <div class="card col-md-6 p-3 text-center h-100">
                                                    <div class="card-header">
                                                        {{ $movie['title'] }}
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="mars_{{ $movie['id'] }}" style="cursor: pointer">
                                                            <img src="{{ asset($movie['img_url']) }}" class="card-img-top" alt="..." />
                                                        </label>
                                                        <input type="radio" name="movieSelection" id="mars_{{ $movie['id'] }}" value="{{ $movie['id'] }}">
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="card-text">
                                                            {{ $movie['description'] }}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                <div class="collapse collapseMovies" id="TBT">
                                    <label for="mars_movie">Select Your Movie</label>
                                    <label for="mars_movie">The Biggest Theatre</label>
                                    <div class="row radio text-center">
                                        @foreach($data['mars_movies'] as $movie)
                                            @if($movie['alias'] == 'TBT')
                                                <div class="card col-md-6 p-3 text-center h-100">
                                                    <div class="card-header">
                                                        {{ $movie['title'] }}
                                                    </div>
                                                    <div class="card-body">
                                                        <label for="mars_{{ $movie['id'] }}" style="cursor: pointer">
                                                            <img src="{{ asset($movie['img_url']) }}" class="card-img-top" alt="..." />
                                                        </label>
                                                        <input type="radio" name="movieSelection" id="mars_{{ $movie['id'] }}" value="{{ $movie['id'] }}">
                                                    </div>
                                                    <div class="card-footer">
                                                        <p class="card-text">
                                                            {{ $movie['description'] }}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="mars_timeslot">Select Your Time Slot</label>
                                <input name="date_booked" type="date" min="{{ date("Y-m-d") }}" max="2021-10-31" class="form-control timeslot_date" required>
                                <select name="time_slot_id" class="form-control timeslot_time" required>
                                </select>
                                <div class="alert alert-danger" style="display: none" id="fullyBooked">
                                    <p>Sorry, date fully booked</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mars_tickets">Select Number Of Tickets</label>
                                <input name="ticket_count" type="range" min="1" max="30" value="1" class="form-control-range tickets" onInput="$('#mars_rangeval').html($(this).val())">
                            </div>
                            <div class="form-group text-center">
                                <span id="mars_rangeval">1</span>
                            </div>
                            <div>
                                <input class="bookTicket" type="submit" value="Book Now">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
