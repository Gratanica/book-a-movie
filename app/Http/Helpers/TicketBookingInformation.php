<?php

namespace App\Http\Helpers;

use App\Models\Movie;
use App\Models\Timeslot;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TicketBookingInformation
{
    /**
     * @var mixed
     */
    protected $time_slot_id;

    /**
     * @var mixed
     */
    protected $ticket_count;

    /**
     * @var
     */
    protected $time_slot;

    /**
     * @var mixed
     */
    protected $theater;

    /**
     * @var mixed
     */
    protected $movie_id;

    /**
     * TicketBookingInformation constructor.
     * @param $post_data
     */
    public function __construct($post_data)
    {
        $this->time_slot_id = Arr::get($post_data, 'time_slot_id');
        $this->ticket_count = Arr::get($post_data, 'ticket_count');
        $this->theater = Arr::get($post_data, 'theater');
        $this->movie_id = Arr::get($post_data, 'movieSelection');
    }

    /**
     * @return bool
     */
    public function isSlotAvailable()
    {
        $this->set_slot();

        $available_seats = $this->time_slot->seat_limit - $this->time_slot->seats_taken;

        if($this->ticket_count <= $available_seats)
        {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getBookInfo()
    {
        return [
            'theater' => $this->theater,
            'movie_start_time' => $this->time_slot->time_start,
            'slot_id' => $this->time_slot_id,
            'ticket_count' => $this->ticket_count,
            'reference' => $this->generateReference(),
            'movie_info' => $this->getMovieInformation(),
        ];
    }

    /**
     * @return mixed
     */
    public function updateTimeSlot()
    {
        return Timeslot::updateOrCreate(['id' => $this->time_slot_id],
            [
                'seats_taken' => $this->time_slot->seats_taken + $this->ticket_count
            ]);
    }

    /**
     * @return array
     */
    private function getMovieInformation()
    {
        $movie = Timeslot::find($this->time_slot_id)->movie;

        return [
            'title' => $movie->title,
            'description' => $movie->description,
            'img_url' => $movie->img_url,
            'theater_id' => $movie->theater_id,
        ];
    }

    /**
     * @return string
     */
    private function generateReference()
    {
        $random = Str::random(30);
        return "theater-{$random}";
    }

    /**
     *
     */
    private function set_slot()
    {
        $this->time_slot = Timeslot::find($this->time_slot_id);
    }
}
