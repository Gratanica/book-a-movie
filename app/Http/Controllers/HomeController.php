<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Theater;
use App\Models\Movie;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jupiter_theaters = Theater::getTheaterByLocation('Jupiter')->get();
        $mars_theaters = Theater::getTheaterByLocation('Mars')->get();

        $jupiter_theater_movies = Movie::getMoviesByTheater($jupiter_theaters->pluck('id')->toArray())->get();
        $mars_theater_movies = Movie::getMoviesByTheater($mars_theaters->pluck('id')->toArray())->get();

        $booking_result = [
            'message_type' => null
        ];

        //return array of data to make homepage display data specific items.
        $data = [
            'jupiter_theaters' => $jupiter_theaters,
            'jupiter_movies' => $jupiter_theater_movies,
            'mars_theaters' => $mars_theaters,
            'mars_movies' => $mars_theater_movies,
        ];

        return view('index', compact('data', 'booking_result'));
    }
}
