<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboardCancellation()
    {
        //is the user logged in
        if (Auth::check())
        {
            //get users tickets booked, that can still be cancelled
            $tickets_can_cancel = Booking::query()
                ->join('timeslots as t', 't.id', '=', 'bookings.timeslot_id')
                ->join('movies as m', 'm.id', '=', 't.movie_id')
                ->where('bookings.user_id', '=', Auth::id())
                ->where('t.time_start', '>', DB::raw('current_timestamp() + interval 60 Minute'))
                ->select(
                    'bookings.id as booking_id',
                    'm.title as movie_name',
                    'bookings.ticket_count',
                    't.time_start'
                )
                ->get();

            $tickets = $tickets_can_cancel;

            $message = [
                'message_type' => null,
                'message' => null
            ];

            return view('cancellations', compact('tickets', 'message'));
        }

        //user not authenticated, we go to homepage
        return redirect()->guest('/');
    }

    public function dashboard()
    {
        //is the user logged in
        if (Auth::check())
        {
            //get users tickets booked
            $bookings = collect(Booking::query()->where('user_id', '=', Auth::id())
                ->get()->map(function ($booking){
                    return Booking::ticketInfo($booking->timeslot_id)->get();
            }))->flatten()->toArray();

            return view('dashboard', compact('bookings'));
        }

        //user not authenticated, we go to homepage
        return redirect()->guest('/');
    }
}
