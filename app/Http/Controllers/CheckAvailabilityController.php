<?php

namespace App\Http\Controllers;

use App\Models\Timeslot;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CheckAvailabilityController extends Controller
{
    public function checkTimeSlotAvailableSeats()
    {
        if(isset($_POST))
        {
            $slot_id = Arr::get($_POST, 'slot');

            $time_slot = Timeslot::find($slot_id);

            return json_encode([
                'available_seats' => (int) $time_slot->seat_limit - (int) $time_slot->seats_taken,
            ]);
        }
    }

    public function checkDateAvailability()
    {
        if(isset($_POST))
        {
            $booking_date = Arr::get($_POST, 'date');
            $movie_id = Arr::get($_POST, 'movie_id');

            //now check if this movie on the specific date is available
            $time_slots = collect(Timeslot::query()->where('movie_id', '=', $movie_id)
                ->where('time_start', 'like', $booking_date.'%')
                ->where('seat_limit', '!=', 'seats_taken')
                ->select(
                    'id as times_slot_id',
                    'seats_taken',
                    'seat_limit',
                    'time_start'
                )->get())->map(function($item)
                {
                    return [
                        'times_slot_id' => $item->times_slot_id,
                        'slot' => Carbon::make($item->time_start)->format('H:i:s'),
                    ];
                })->toArray();

            //check if no seats available for that date.
            if(empty($time_slots))
            {
                return json_encode(['is_available' => false]);
            }

            return json_encode([
                'time_slots' => $time_slots,
                'is_available' => true
            ]);
        }

        return json_encode([
            'message' => 'no data posted, please try again.'
        ]);
    }
}
