<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Timeslot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CancelBookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        //is the user logged in
        if (Auth::check())
        {
            if(is_null($request->all()))
            {
                //get users tickets booked, that can still be cancelled
                $tickets_can_cancel = Booking::query()
                    ->join('timeslots as t', 't.id', '=', 'bookings.timeslot_id')
                    ->join('movies as m', 'm.id', '=', 't.movie_id')
                    ->where('bookings.user_id', '=', Auth::id())
                    ->where('t.time_start', '>', DB::raw('current_timestamp() + interval 60 Minute'))
                    ->select(
                        'bookings.id as booking_id',
                        'm.title as movie_name',
                        'bookings.ticket_count',
                        't.time_start'
                    )
                    ->get();

                $tickets = $tickets_can_cancel;
                $message = [
                    'message_type' => 'no data',
                    'message' => 'No data received on your request to cancel.'
                ];

                return view('cancellations', compact('tickets', 'message'));
            }

            //here we get the booking_id for cancellation.
            $booking_id = $request->get('cancel_id');

            if(is_null($booking_id))
            {
                //get users tickets booked, that can still be cancelled
                $tickets_can_cancel = Booking::query()
                    ->join('timeslots as t', 't.id', '=', 'bookings.timeslot_id')
                    ->join('movies as m', 'm.id', '=', 't.movie_id')
                    ->where('bookings.user_id', '=', Auth::id())
                    ->where('t.time_start', '>', DB::raw('current_timestamp() + interval 60 Minute'))
                    ->select(
                        'bookings.id as booking_id',
                        'm.title as movie_name',
                        'bookings.ticket_count',
                        't.time_start'
                    )
                    ->get();

                $tickets = $tickets_can_cancel;

                $message = [
                    'message_type' => 'no data',
                    'message' => 'No booking data received on your request to cancel.'
                ];

                return view('cancellations', compact('tickets', 'message'));
            }

            $booking_info = Booking::find($booking_id);

            //we now delete the booking
            $result = Booking::where('id', $booking_id)->delete();

            $timeslot_info = Timeslot::where('id', $booking_info->timeslot_id)->first();

            $seats_left = (int) $booking_info->ticket_count - $timeslot_info->seats_taken;

            if($result)
            {
                //we now update the timeslot seats_taken
                Timeslot::where('id', $booking_info->timeslot_id)
                    ->update(['seats_taken' => $seats_left]);

                //get users tickets booked, that can still be cancelled
                $tickets_can_cancel = Booking::query()
                    ->join('timeslots as t', 't.id', '=', 'bookings.timeslot_id')
                    ->join('movies as m', 'm.id', '=', 't.movie_id')
                    ->where('bookings.user_id', '=', Auth::id())
                    ->where('t.time_start', '>', DB::raw('current_timestamp() + interval 60 Minute'))
                    ->select(
                        'bookings.id as booking_id',
                        'm.title as movie_name',
                        'bookings.ticket_count',
                        't.time_start'
                    )
                    ->get();

                $tickets = $tickets_can_cancel;

                $message = [
                    'message_type' => 'success',
                    'message' => "Successfully cancelled ticket reference {$booking_info->ticket_reference}."
                ];

                return view('cancellations', compact('tickets', 'message'));
            }

            //get users tickets booked, that can still be cancelled
            $tickets_can_cancel = Booking::query()
                ->join('timeslots as t', 't.id', '=', 'bookings.timeslot_id')
                ->join('movies as m', 'm.id', '=', 't.movie_id')
                ->where('bookings.user_id', '=', Auth::id())
                ->where('t.time_start', '>', DB::raw('current_timestamp() + interval 60 Minute'))
                ->select(
                    'bookings.id as booking_id',
                    'm.title as movie_name',
                    'bookings.ticket_count',
                    't.time_start'
                )
                ->get();

            $tickets = $tickets_can_cancel;
            $message = [
                'message_type' => 'error',
                'message' => 'Failed to cancel ticket.'
            ];

            return view('cancellations', compact('tickets', 'message'));
        }

        //user not authenticated, we go to homepage
        return redirect()->guest('/');
    }
}
