<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Movie;
use App\Models\Theater;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\TicketBookingInformation;

class BookTicket extends Controller
{
    //here we are going to book the ticket.
    public function index(Request $request)
    {
        try
        {
            //if no post data, redirect to home page and display movies all over
            if(is_null($request->all()))
            {
                $jupiter_theaters = Theater::getTheaterByLocation('Jupiter')->get();
                $mars_theaters = Theater::getTheaterByLocation('Mars')->get();

                $jupiter_theater_movies = Movie::getMoviesByTheater($jupiter_theaters->pluck('id')->toArray())->get();
                $mars_theater_movies = Movie::getMoviesByTheater($mars_theaters->pluck('id')->toArray())->get();

                //return array of data to make homepage display data specific items.
                $data = [
                    'jupiter_theaters' => $jupiter_theaters,
                    'jupiter_movies' => $jupiter_theater_movies,
                    'mars_theaters' => $mars_theaters,
                    'mars_movies' => $mars_theater_movies,
                ];

                $booking_result = [
                    'message' => 'no values received.',
                    'message_type' => 'no data'
                ];

                return view('index', compact('booking_result','data'));
            }

            $booking = new TicketBookingInformation( $request->all() );

            //if booking slot is still available, redirect to home page and display movies all over
            if(!$booking->isSlotAvailable())
            {
                $jupiter_theaters = Theater::getTheaterByLocation('Jupiter')->get();
                $mars_theaters = Theater::getTheaterByLocation('Mars')->get();

                $jupiter_theater_movies = Movie::getMoviesByTheater($jupiter_theaters->pluck('id')->toArray())->get();
                $mars_theater_movies = Movie::getMoviesByTheater($mars_theaters->pluck('id')->toArray())->get();

                //return array of data to make homepage display data specific items.
                $data = [
                    'jupiter_theaters' => $jupiter_theaters,
                    'jupiter_movies' => $jupiter_theater_movies,
                    'mars_theaters' => $mars_theaters,
                    'mars_movies' => $mars_theater_movies,
                ];

                $booking_result = [
                    'message' => 'Slot no longer available, please select a new time.',
                    'message_type' => 'no data'
                ];

                return view('index', compact('booking_result','data'));
            }

            $booking_info = $booking->getBookInfo();

            //is the user logged in
            if (Auth::check())
            {

                /*Here we make a booking*/
                $user_id = Auth::id();

                $result = Booking::create([
                    'user_id' => $user_id,
                    'timeslot_id' => Arr::get($booking_info, 'slot_id'),
                    'ticket_count' => Arr::get($booking_info, 'ticket_count'),
                    'ticket_reference' => Arr::get($booking_info, 'reference'),
                ]);

                if($result)
                {
                    // Update the timeslots existing number of bookings
                    $booking->updateTimeSlot();

                    $booking_result = [
                        'booking_reference' => Arr::get($booking_info, 'reference'),
                        'movie_info' => Arr::get($booking_info, 'movie_info'),
                    ];

                    //if booking was successfull, we display the success page with ticket information
                    return view('success', compact('booking_result'));
                }

                //redirect back to the
                $jupiter_theaters = Theater::getTheaterByLocation('Jupiter')->get();
                $mars_theaters = Theater::getTheaterByLocation('Mars')->get();

                $jupiter_theater_movies = Movie::getMoviesByTheater($jupiter_theaters->pluck('id')->toArray())->get();
                $mars_theater_movies = Movie::getMoviesByTheater($mars_theaters->pluck('id')->toArray())->get();

                //return array of data to make homepage display data specific items.
                $data = [
                    'jupiter_theaters' => $jupiter_theaters,
                    'jupiter_movies' => $jupiter_theater_movies,
                    'mars_theaters' => $mars_theaters,
                    'mars_movies' => $mars_theater_movies,
                ];

                $booking_result = [
                    'message' => 'Failed to process booking, please try again.',
                    'message_type' => 'error'
                ];

                return view('index', compact('booking_result', 'data'));

            }

            /*if not logged in we redirect to login page.*/
            return redirect()->route('login');


        }catch(\Exception $exception)
        {
            $jupiter_theaters = Theater::getTheaterByLocation('Jupiter')->get();
            $mars_theaters = Theater::getTheaterByLocation('Mars')->get();

            $jupiter_theater_movies = Movie::getMoviesByTheater($jupiter_theaters->pluck('id')->toArray())->get();
            $mars_theater_movies = Movie::getMoviesByTheater($mars_theaters->pluck('id')->toArray())->get();

            //return array of data to make homepage display data specific items.
            $data = [
                'jupiter_theaters' => $jupiter_theaters,
                'jupiter_movies' => $jupiter_theater_movies,
                'mars_theaters' => $mars_theaters,
                'mars_movies' => $mars_theater_movies,
            ];

            $booking_result = [
                //'message' => 'An unexpected error occurred.',
                'message' => $exception->getMessage(),
                'message_type' => 'error'
            ];

            return view('index', compact('booking_result', 'data'));
        }
    }
}
