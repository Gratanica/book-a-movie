<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Theater extends Model
{
    /**
     * Get the movies associated with the Theater.
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }


    /**
     * Get location based theaters.
     */
    public function scopegetTheaterByLocation($query, $theater)
    {
        return $query->where('location_id', '=',
            Location::query()->where('name','=', $theater)->first()->id
        )->select(
            'id',
            'location_id',
            'name',
            'alias'
        );
    }

}
