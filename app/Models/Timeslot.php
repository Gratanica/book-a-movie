<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    /**
     * Get the timeslots associated with the booking.
     */
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * Get the movie associated with the timeslot.
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    protected $guarded = [];
}
