<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Movie extends Model
{
    /**
     * Get the cinemas associated with the Movie.
     */
    public function theaters()
    {
        return $this->belongsTo(Theater::class);
    }

    /**
     * Get the timeslots associated with the movie.
     */
    public function timeslots()
    {
        return $this->hasMany(Timeslot::class);
    }

    public function scopegetMoviesByTheater($query, $theater_ids)
    {
        return $query->join('theaters', 'theaters.id', '=', 'movies.theater_id')
            ->whereIn('theater_id', $theater_ids)
            ->select(
            'movies.id',
            'title',
            'alias',
            'description',
            'img_url'
        );
    }
}
