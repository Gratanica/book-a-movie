<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    /**
     * Get the timeslots associated with the booking.
     */
    public function timeSlot()
    {
        return $this->belongsTo(Timeslot::class);
    }

    public function scopeticketInfo($query, $slot_id)
    {
        return $query->join('timeslots as ts', 'ts.id', '=', 'bookings.timeslot_id')
            ->join('movies as m', 'm.id', '=', 'ts.movie_id')
            ->join('theaters as t', 't.id', '=', 'm.theater_id')
            ->join('locations as l', 'l.id', '=', 't.location_id')
            ->where('bookings.timeslot_id','=', $slot_id)
            ->select(
                'm.title',
                'bookings.ticket_count as tickets',
                'ts.time_start as start_time',
                'l.name as location',
                't.name as theater',
                'bookings.created_at as date',
                DB::raw('CASE 
                            WHEN 
                                "2021-10-04 01:31:32" BETWEEN current_timestamp() + interval 1 hour AND current_timestamp()
                            THEN
                                concat(TIMEDIFF("time", current_timestamp()), " Minutes to go (no cancellation)")
                            WHEN 
                                "2021-10-04 01:31:32" > current_timestamp() + interval 1 hour
                            THEN 
                                concat(DATEDIFF("date", current_timestamp())," Days to go")
                            ELSE
                                "Hope you enjoyed your movie!"
                        END as status'
                ),
                'bookings.ticket_reference as reference'
            );
    }

    protected $guarded = [];
}
